<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/news', ['as' => 'news.all', 'uses' => 'NewsController@all']);
$router->get('/news/{id}', ['as' => 'news.one', 'uses' => 'NewsController@one']);
$router->post('/news', ['as' => 'news.insert', 'uses' => 'NewsController@insert']);
$router->put('/news/{id}', ['as' => 'news.full-update', 'uses' => 'NewsController@fullUpdate']);
$router->patch('/news/{id}', ['as' => 'news.update', 'uses' => 'NewsController@update']);
$router->delete('/news/{id}', ['as' => 'news.delete', 'uses' => 'NewsController@delete']);
