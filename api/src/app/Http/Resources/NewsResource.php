<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'excerpt' => $this->excerpt,
            'content' => $this->content,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            '_links' => [
                'news.all' => ['href' => route('news.all'), 'method' => 'GET', 'templated' => false],
                'news.one' => ['href' => route('news.one', ['id' => $this->id]), 'method' => 'GET', 'templated' => false],
                'news.insert' => ['href' => route('news.insert', ['id' => $this->id]), 'method' => 'POST', 'templated' => false],
                'news.full-update' => ['href' => route('news.full-update', ['id' => $this->id]), 'method' => 'PUT', 'templated' => false],
                'news.update' => ['href' => route('news.update', ['id' => $this->id]), 'method' => 'PATCH', 'templated' => false],
                'news.delete' => ['href' => route('news.delete', ['id' => $this->id]), 'method' => 'DELETE', 'templated' => false],

            ] //for HATEOAS Driven REST APIs
        ];
    }
}
