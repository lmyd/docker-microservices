<?php

namespace App\Http\Controllers;

use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Services\NewsService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class NewsController extends Controller
{
    /** @var NewsService */
    protected $service;

    /**
     * NewsController constructor.
     * @param NewsService $service
     */
    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return NewsCollection
     */
    public function all()
    {
        return $this->service->getAll();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return NewsResource
     */
    public function one(int $id)
    {
        try {
            $item = $this->service->getOne($id);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
        return $item;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return NewsResource
     * @throws ValidationException
     */
    public function insert(Request $request)
    {
        $data = $this->validate($request, [
            'title' => 'required|max:255',
            'excerpt' => 'required',
            'content' => 'required',
        ]);
        return $this->service->insert($data['title'], $data['excerpt'], $data['content']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return NewsResource
     * @throws ValidationException
     */
    public function fullUpdate(Request $request, int $id)
    {
        $data = $this->validate($request, [
            'title' => 'required|max:255',
            'excerpt' => 'required',
            'content' => 'required',
        ]);
        try {
            $item = $this->service->update($id, $data['title'], $data['excerpt'], $data['content']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return NewsResource
     */
    public function update(Request $request, int $id)
    {
        try {
            $item = $this->service->update(
                $id,
                $request->input('title'),
                $request->input('excerpt'),
                $request->input('content')
            );
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
        return $item;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array
     */
    public function delete(int $id)
    {
        try {
            $this->service->delete($id);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
        return ['deleted news ' . $id];
    }
}
