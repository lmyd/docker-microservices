<?php

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Models\News;

class NewsService
{
    /**
     * All news collection
     *
     * @return NewsCollection
     */
    public function getAll(): NewsCollection
    {
        return new NewsCollection(News::all());
    }

    /**
     * Get one news
     *
     * @param int $id
     *
     * @return NewsResource
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): NewsResource
    {
        $news = News::find($id);
        if (!$news instanceof News)
            throw new NotFoundException('News with id ' . $id . ' not found.');

        return new NewsResource($news);
    }

    /**
     * Insert one news
     *
     * @param string $title
     * @param string $excerpt
     * @param string $content
     * @return NewsResource
     */
    public function insert(string $title, string $excerpt, string $content): NewsResource
    {
        $news = new News;
        $news->title = $title;
        $news->excerpt = $excerpt;
        $news->content = $content;
        $news->save();
        return new NewsResource($news);
    }

    /**
     * Update single news
     *
     * @param int $id
     * @param string|null $title
     * @param string|null $excerpt
     * @param string|null $content
     * @return NewsResource
     * @throws NotFoundException
     */
    public function update(int $id, string $title = null, string $excerpt = null, string $content = null): NewsResource
    {
        $news = News::find($id);

        if (!$news instanceof News)
            throw new NotFoundException('News with id ' . $id . ' not found.');

        if (!is_null($title))
            $news->title = $title;

        if (!is_null($excerpt))
            $news->excerpt = $excerpt;

        if (!is_null($content))
            $news->content = $content;

        $news->save();
        return new NewsResource($news);
    }

    /**
     * Delete single news
     *
     * @param int $id
     * @throws NotFoundException
     */
    public function delete(int $id)
    {
        $news = News::find($id);

        if (!$news instanceof News)
            throw new NotFoundException('News with id ' . $id . ' not found.');

        $news->delete();
    }
}
