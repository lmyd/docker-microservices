# Run it:

* git clone git@bitbucket.org:lmyd/docker-microservices.git
* cd docker-microservice
* docker-compose build
* docker-compose up -d
* docker-compose exec php composer install -d/var/www/html/
* docker-compose exec php php /var/www/html/artisan migrate

# Services:

* http://localhost:8089/ => app
* http://localhost:8088/ => api


# Description

1. Kontener mysql - baza danych MySQL/MariaDB
2. Kontener php-fpm - php 7.2

    * Odzielny kontener na php w celu mozliwosci odpalania np api na roznych wersjach php pomocne w przypadku migracji na nowsza wersje
    
3. Kontener api - aplikacja backendowa PHP oparta na:
    * PHP 7.2 & composer
    * MicroFramework Lumen
    * Eloquent ORM do polaczenia z MySql
    * Nginx http serwer
4. Kontener app - aplikacja frontendowa JavaScript typu SPA oparta na:
    * Node & npm
    * VueJs
    * NuxtJs micro framework
    * plugin Vue-Bootstrap

# Task content 

Przygotować aplikację wyświetlającą dane pobrane za pomocą API.

Zadaniem jest przygotowanie:

* bazy danych opartej o MySQL/MariaDB
* aplikacji “wystawiającej” API
* aplikacji pobierającej dane z API i wyświetlającej je w dowolnej formie np. listing newsów (tytuł, zajawka, data publikacji)

Wymagania:

* API powinno być zrealizowane za pomocą gotowego (gotowe rozwiązanie zapewniające REST API) lub semi-gotowego (np. framework do budowy API) rozwiązania
* W przypadku gotowego rozwiązania nie musi być ono oparte o PHP, ale musi to być język programistyczny znany przez kandydata
* API ma zapewniać podstawowe operacje CRUD
* całość powinna być oparta o kontenery dockerowe (kontener DB, kontener API, kontener APP) z możliwością uruchomienia ich za pomocą docker-compose
* aplikacja wyświetlająca dane powinna być zrealizowana za pomocą jednego z microframeworków PHP lub czystego PHP

Uwagi:

* struktura danych udostępnianych przez API jest dowolna; może to być najprostszy zestaw danych np. id, title, excerpt, created_at
* całość powinna być opatrzona komentarzem (plik README) na temat wybranych rozwiązań (API, microframework) - jakie rozwiązania były brane pod uwagę i dlaczego akurat te zostały wykorzystane
